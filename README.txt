Foundations of Informatics INFO20002
By Chris Bradley, Nicholas Brown, Seungwoo Kim

To run the application, just serve index.html found in the root directory. All the other application components can be accessed through the links it displays.

sanitisation.py is a program which takes disaster_original.csv and converts it into disaster_processed.csv (same directory as the program).

Git repository link: https://bitbucket.org/MrPherein/info20002-phase3-2014/overview