AIM:

	FINISH PHASE 1 BY 21/05/2014

USEFUL LINKS:

	https://sites.google.com/site/info2002y14s1/project-specification/phase3
	https://sites.google.com/site/info2002y14s1/project-specification/week10-activities
	http://data.gov.au/dataset/disaster-events-with-category-impact-and-location/resource/ad5c6594-571e-4874-994c-a9f964d789df
	http://students.informatics.unimelb.edu.au/~ivow/foi/mywork/solution/phase3/form.py
	http://students.informatics.unimelb.edu.au/~ivow/foi/mywork/solution/phase3/pivot.py?attr1=State&attr2=Year&values=Population&filter=Year&filter_value=

TO TEST LOCALLY

	1. Navigate to phase1 directory
	2. Execute cgiserver.py
	3. Navigate to http://127.0.0.1/cgi-bin/interface.py.

PLAN:

	Phase1
	Tasks:
		Python script to sanitise/clean data				Seung Woo
		Python script to and send values to script 1			Nicholas
		Python script to generate pivot table				Chris
		Design HTML/css

	Phase 2
	Tasks:
		Wait until phase 1 finished
		Design application
		Choose interactivity vs visualisations
		Identify similarities between questions to avoid code duplication
		Do questions.
    Questions:
		1. Most frequent disaster types in states		Absolute number of disasters types, between states						Clustered bar graph
		2. Amount of disasters in states				Absolute number of disasters comparison between states					Bar graph
			CHANGED -	Amount of disasters			Absolute number of disasters
		3. Deadliest disaster types						Average amount of deaths per disaster type								Bar graph
		4. Most costly disasters						Average insurance cost per disaster type								Bar graph
		5. Frequency over time of type					Number of disasters for each type per year (perhaps decade binning?)	Line graph
	Questions that we aren't doing:
		6. Natural vs Man made over time
		7. Most damaging disasters (?)
		8. Longest distasters (end - start time)
	Ideas:
	-Graphs
		-Bar
		-Choropleth
		-Map infographic
		
	Tasks (the dice have spoken):
		Clustered bar graph generation										Chris
		Bar graph generation												Nicholas
		Line graph generation												Seungwoo
		Interface (AKA, what to chart - can use same CSS as P1 interface)	Chris
		Display page														Seungwoo
		Presentation														Nicholas

	Notes:
		Meetup to finalise project:
			Tuesday 27th May - 3:30 - Alice Hoy
		Python programs to generate the visualisations should be separate to prevent conflicts

TODO:
	pivot.py:
		-Add colouring
		-Do CSS - make it look like it came from this century
		-Untangle display and generation logic (Separation of concerns anyone?)
		-Test other combinations once data cleaning comes online

	NEW: 
	-Aggregate data colouring pivot chart (Chris)
	-Add descriptions to charts			  (Everyone)
		-Update CSS