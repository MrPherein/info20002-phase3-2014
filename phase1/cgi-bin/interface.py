#!/usr/bin/env python

"""

An interface for the pivot table, where the user can select the row, column,
display value and filter.

"""

import utils


def displaySelect(name, sel_type, title=None, selected=""):
    """
    Generates an xhtml select/dropdown box with the name, 'name'. Also
    outputs a description of the box, given by title.

    Selected indicates what the default value should be.

    Filter indicates whether the select is a filter for the pivot table.
    """
    if title is not None:
        print '<h2>{}</h2>'.format(title)
    print '<select name="{}">'.format(name)

    #Determine which options to use
    if sel_type == utils.sel_filter:
        options = utils.filter_cat
    elif sel_type == utils.sel_gen:
        options = utils.gen_cat
    elif sel_type == utils.sel_val:
        options = utils.val_cat
    else:
        options = utils.ops

    #Output xhtml
    for k, v in sorted(options.items(), key=lambda x: (x[1], x[0])):
        if k != selected:
            print '<option value="{}">{}</option>'.format(k, v)
        else:
            print ('<option value="{}" '.format(k) +
                   'selected="selected">{}</option>'.format(v))

    print '</select>'


def displayInputText(name):
    """
    Generates an xhtml text field with the name, 'name'.
    """
    print '<input type="text" name="{}" />'.format(name)


def displayForm():
    """
    Generate the xhtml form.
    """
    #Using get, as post does not function on IVLE
    print '<form method="get" action="{}">'.format(utils.pivot_file)

    displaySelect(utils.name_row, utils.sel_gen, "Rows", utils.default_row)
    displaySelect(utils.name_col, utils.sel_gen, "Columns", utils.default_col)
    displaySelect(utils.name_val, utils.sel_val, "Values", utils.default_val)

    print '<h2>Filters</h2>'
    for i in xrange(utils.max_filters):
        displaySelect(utils.name_filter + str(i), utils.sel_filter,
                      selected=utils.default_filter)
        displaySelect(utils.name_op + str(i), utils.sel_op,
                      selected=utils.default_op)
        displayInputText(utils.name_filter_value + str(i))
        print '<br />'
    print '<br />'

    print '<p><input type="submit"/></p>\n</form>'
    print '<br />'


def displayDataSource():
    """
    Display the source URL for the data.
    """
    print ('<p class="source"><em>Data used for the pivot table can be found '
           '<a href="{}">here</a></em></p>'.format(utils.source_url))


def displayBody():
    """
    Display the body of an xhtml page.
    """
    print '<body>'
    print '<h1>Pivot Table for Disaster Events</h1>'
    print ('<p>Please select the desired rows, columns, values and '
           'filters for the pivot table:</p>')
    displayForm()
    displayDataSource()
    print '</body>'


def displayPage():
    """
    Display the xhtml page.
    """
    utils.xhtmlHead(utils.inter_title, "../style/interface.css")
    displayBody()
    utils.xhtmlFoot()


def main():
    displayPage()

if __name__ == "__main__":
    main()
