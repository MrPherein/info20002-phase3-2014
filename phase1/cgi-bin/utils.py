#Title of pivot table interface
inter_title = "Pivot Table - Interface"

#Name of pivot table generation file
pivot_file = "pivot.py"

#Name of interface file
interface_file = "interface.py"

#Location of the csv file
csv_file = "../../disaster_processed.csv"

#Maximum number of filters
max_filters = 3

#Name of cgi elements (general)
name_row = "attrrow"
name_col = "attrcol"
name_val = "attrval"

#These cgi elements should be appended with a number, starting from 0
#and incrementing by 1 until max_filters - 1 is reached to represent all the
#filters
name_filter = "filter"
name_filter_value = "filter_value"
name_op = "op"

#Select flags
sel_gen, sel_val, sel_filter, sel_op = range(4)

#Data categories - {name of xhtml element: description to output}
gen_cat = {"type": "Type", "year": "Year", "region": "Region"}

val_cat = {"deaths": "Deaths", "injuries": "Injuries",
           "evacuated": "Evacuated",
           "insured_cost": "Insured Cost",
           "homes_destroyed": "Home(s) Destroyed",
           "buildings_destroyed": "Building(s) Destroyed",
           "homes_damaged": "Home(s) Damaged",
           "buildings_destroyed": "Building(s) Destroyed",
           "aircraft_destroyed": "Aircraft Destroyed",
           "motor_vechicles_destroyed": "Motor Vechicle(s) Destroyed",
           "water_vessels_destroyed": "Water Vessel(s) Destroyed",
           "motor_vechicles_damaged": "Motor Vechicle(s) Damaged",
           "water_vessels_damaged": "Water Vessel(s) Damaged",
           "livestock_destroyed": "Livestock Destroyed",
           "government_assistance": "Government Assistance"}

filter_cat = {"none": "None"}
filter_cat.update(val_cat)

#Filter operators
ops = {"lt": "Less Than", "eq": "Equal To", "gt": "Greater Than"}

#Default category for the pivot table interface
default_row = "type"
default_col = "year"
default_val = "deaths"
default_filter = "none"
default_op = "eq"

#Source URL for disaster data
source_url = ("http://www.data.gov.au/dataset/"
              "disaster-events-with-category-impact-and-location/"
              "resource/ad5c6594-571e-4874-994c-a9f964d789df")


def xhtmlHead(title, css):
    """
    Print a xhtml header.
    """
    print ('Content-Type: text/html\n\n'
           '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" '
           '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n'
           '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" '
           'lang="en">\n'
           '<head>\n<meta http-equiv="content-type" content="text/html; '
           'charset=UTF-8" />\n'
           '<link rel="stylesheet" type="text/css" href="{}" '.format(css) +
           '/>\n'
           '<title>{}</title>\n'.format(title) +
           '</head>')


def xhtmlFoot():
    """
    Print a xhtml footer.
    """
    print '</html>'
