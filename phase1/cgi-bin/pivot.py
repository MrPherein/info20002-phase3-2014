#!/usr/bin/env python

"""

Generates a pivot table based off parameters received over cgi.
Specify the rows, columns and filters to apply to the data.
Error handling for missing parameters, and returning no data.

"""

import csv
import cgi
import utils
import operator
from colour import Color
from collections import defaultdict

def printError(errormsg):
    """
    Displays an error page with a link back to interface.py
    """
    print '<div class="error"><h1>{}</h1></div>'.format(errormsg)
    print """
        <div class="inner">
        <form action="{}">
            <input class="errorbutton" type="submit" value="Click here to return"/>
        </form>
        </div>""".format(utils.interface_file)

def generate_pivot(row,col,filters,pval):
    """
    Returns a defaultdict of defaultdicts which represents a pivot chart.
    Uses specified row and col values and filters individual pieces of data
    based off filters. Final data aggregate is not filtered, only the original
    components. pval specifies the type of aggregate returned, ie deaths or costs.
    """
    pivot = []

    # Read csv data in from specified file in utils
    with open(utils.csv_file,"rb") as csvfile:
        reader = csv.DictReader(csvfile)
        row_data = defaultdict(lambda: defaultdict(int))

        # { type : {year : count} }
        # Get data
        for line in reader:
            allow = True
            for filter in filters:
                # Ignore none filters, if set value in interface then it is passed on
                # So we ignore it here.
                if filter[0] == 'none':
                    continue
                if isNumber(filter[1][0]):
                    if filter[1][1] == 'lt' and int(line[filter[0]]) > int(filter[1][0]):
                        allow = False
                    if filter[1][1] == 'eq' and int(line[filter[0]]) != int(filter[1][0]):
                        allow = False
                    if filter[1][1] == 'gt' and int(line[filter[0]]) < int(filter[1][0]):
                        allow = False
                else:
                    printError("Invalid filter selected. Please try again.")
                    return
            if allow:
                for citem in line[col].split(";"):
                    for ritem in line[row].split(";"):
                        row_data[ritem][citem] += int(line[pval])

        # Get unique column headers
        col_dict = {}
        for header in row_data:
            for item in row_data[header]:
                col_dict[item] = 0
        
        pivot.append([])    # New row for column headers
        pivot[0].append('') # Blank cell in top left
        index = 1           # Start column headers after blank cell
        # Write column headers to pivot list
        for col in sorted(col_dict):
            pivot[0].append(col)
            col_dict[col] = index
            index += 1

        # Start data after header row
        row_cnt = 1         
        # Write out row headers to row and add empty cells to write to later
        for header in sorted(row_data):
            pivot.append([0]*(len(col_dict) + 1))
            pivot[row_cnt][0] = header
            row_cnt += 1
        
        # Write out data to list
        row_cnt = 1
        for header in sorted(row_data):
            for item in row_data[header]:
                pivot[row_cnt][col_dict[item]] = row_data[header][item]
            row_cnt += 1

    return pivot

def display_pivot(pivot):
    """
    Output the previously generated pivot table as a html table. Use th elements
    for table header on top and far left row. Everything else is a td element.
    """
    # Make sure we're not printing an empty table
    if len(pivot) == 1:
        printError("Error: No data returned. Change your filters and try again.")
        return

    # Open table
    print "<table>"
    # Print first row of table headers
    for line in pivot[0:1]:
        print "<tr>"
        for cell in line:
            print "<th>{}</th>".format(cell)
        print "</tr>"

    # Get max value in table cells, used later for cell colouring
    max = 0
    for line in pivot[1:]:
        for cell in line[1:]:
            if int(cell) > max:
                max = int(cell)

    # Print main table content, first cell of each row is a table header
    for line in pivot[1:]:
        print "<tr>"
        print "<th>{}</th>".format(line[0])
        for cell in line[1:]:
            print '<td style="background-color: %s; color: %s;">' % (getBGColour(int(cell),max),getFGColour(int(cell),max))
            print '%s</td>' % cell
        print "</tr>"
    print "</table>"

def getFGColour(cur,max):
    """
    Returns a hex string corresponding to the text colour of a cell in the pivot table.
    Light colours for high values, dark for low values and black for 0. Aim for good
    constrast with background colour.
    """
    percent = float(cur) / max
    if cur == 0:
        return Color(hsl=(0,0,0)).hex   #Black text when value is 0
    else:
        if percent < 0.35 or percent > 0.65:
            return Color(hsl=(0,1,percent)).hex # Dynamic colours for edge cases
        else:
            return Color(hsl=(0,0,0)).hex   # Black text for middle values

def getBGColour(cur,max):
    """
    Returns a hex string corresponding to the background colour of a cell in the pivot table.
    Dark colours for high values, light colours for low values. Return a special colour for
    0 cells as we want them to be deemphasised.
    """
    if cur == 0:
        return Color(hsl=(0,0,0.66)).hex
    else:
        return Color(hsl=(0,1,1-(float(cur) / max))).hex

def main():
    """
    Main function. Get pivot chart attributes from cgi. If any are missing display an error
    message. If all attributes are present then display the pivot chart.
    """
    # Send page header
    utils.xhtmlHead(utils.inter_title,"../style/pivot.css")
    print "<body>"
    # Get arguments sent from interface.py
    form = cgi.FieldStorage()

    # Make sure required fields were sent
    # The value of the filter is not checked, as it may not be empty
    attribs_sent = True
    req_attr = [utils.name_row,utils.name_col,utils.name_val]
    for filter_num in xrange(utils.max_filters):
        req_attr.append(utils.name_filter + str(filter_num))
    for attr in req_attr:
        if attr not in form:
            attribs_sent = False
            break

    # If we have all required values then make the pivot table
    if attribs_sent:     
        # Get filters from cgi form if they exist.
        # Format: [(filter_name, (value,type)), ...]
        filters = []
        for filter_num in xrange(utils.max_filters):
            nam_str = utils.name_filter + str(filter_num)
            val_str = utils.name_filter_value + str(filter_num)
            op_str = utils.name_op + str(filter_num)
            if val_str in form and op_str in form:
                filters.append((form[nam_str].value,(form[val_str].value,form[op_str].value)))

        # Get row, column and value type for pivot table from cgi form values
        attrrow = form[utils.name_row].value
        attrcol = form[utils.name_col].value
        attrval = form[utils.name_val].value

        pivot = generate_pivot(attrrow,attrcol,filters,attrval)
        display_pivot(pivot)
    else:
        # If we're missing values then show the user an error and a link to try again
        printError("Error: Required values missing. Please try again.")
    
    print "</body>"
    # Send page footer
    utils.xhtmlFoot()

def isNumber(num):
    """
    Returns true if numerical input is entered, False if not.
    """
    try:
        int(num)
        return True
    except ValueError:
        return False

if __name__ == "__main__":
    main()