#Title of chart interface
inter_title = "Disaster Charts - Interface"

#Location of chart display program
graph_file = "display.py"

#Location of the csv file
csv_file = "../../disaster_processed.csv"

#Location of image directory
img_dir = "images/"

#Chart image file extension
img_extension = ".png"

#Name of cgi elements
name_chart = "chart"
name_gen_y = "yval"

#Data display options for hypotheses
#Contains a chart identifier, with a title and description to display
#in a xhtml page
chart = {"cbar-state-death": {"title": ("Total deaths per disaster type per "
                                        "region"),
                              "description": ("From the chart, it is clear "
                                              "that environmental disasters "
                                              "cause the most amount of "
                                              "deaths out of cyclones, "
                                              "environmental and industrial "
                                              "disasters. What is "
                                              "particularly notable though, "
                                              "is that the total number of "
                                              "deaths are fairly similar "
                                              "across all the regions.<br />"
                                              "<br />When taking into "
                                              "consideration the population, "
                                              "such as Tasmania's 513,400 "
                                              "and Queensland's 4,676,400 (<a"
                                              " href=\"http://www.abs.gov.au"
                                              "/ausstats/abs@.nsf/mf/3101.0\">"
                                              "Australian Bureau of "
                                              "Statistics Estimated "
                                              "Population September 2013</a>"
                                              "), this paints a startling "
                                              "picture.<br /><br />It is "
                                              "unclear as to why each state "
                                              "has similar numbers of deaths "
                                              "despite the differing "
                                              "population levels. One "
                                              "suggestion is that there "
                                              "might be a greater proportion "
                                              "of people suceptible to "
                                              "environmental disasters "
                                              "(the vast majority of which "
                                              "are heatwaves), but there "
                                              "could be other contributing "
                                              "factors.<br /><br />Less "
                                              "surprising are the varying "
                                              "levels of industrial "
                                              "disasters. The regions with "
                                              "the larger number of deaths "
                                              "from such disasters appear to "
                                              "be those with high industrial "
                                              "activity.<br /><br />Cyclone "
                                              "disaster information appears "
                                              "to suggest that the northern "
                                              "regions of Australia are "
                                              "particularly prone to "
                                              "cyclones, with Queensland, "
                                              "Western Australia and "
                                              "Northern Territory (with its "
                                              "small population of 240,000) "
                                              "having a relatively high "
                                              "number of deaths, and the "
                                              "South side having relatively "
                                              "few, if any.")},
         "bar-type": {"title": "Disaster type frequency",
                      "description": ("As can be seen from the chart, the "
                                      "most common types of disasters "
                                      "appear to be related to vehicles "
                                      "with shipwrecks and transport having "
                                      "a relatively high frequency."
                                      "<br /><br />What's especially "
                                      "interesting to see is that these two "
                                      "most common disasters are manmade "
                                      "disasters, and are not in fact "
                                      "natural.<br /><br />That is not to "
                                      "say that natural disasters are "
                                      "inconsequential. As shown in the "
                                      "chart, bushfires, cyclones, floods "
                                      "also have a high frequency. "
                                      "Bushfires are particularly "
                                      "unsurprising, considering "
                                      "Australia's hot, dry climate."
                                      "<br /><br />Landslides, tornados "
                                      "and tsunamis are very infrequent.")},
         "bar-type-deaths": {"title": "Deadliest disasters (average)",
                             "description": ("The most striking component of "
                                             "this graph, is that the deaths "
                                             "caused by tsunamis are "
                                             "approximately two orders of "
                                             "magnitude higher than the "
                                             "second most deadly disaster, "
                                             "earthquakes. Fortunately, the "
                                             "number of tsunamis are amongst "
                                             "the lowest of all disaster "
                                             "types (refer to the "
                                             "'Disaster type frequency'"
                                             "chart).</ br></ br>Also "
                                             "notable is that although they "
                                             "are amongst the most costly "
                                             "(refer to the "
                                             "'Most costly disasters "
                                             "in terms of insurance (average)'"
                                             "chart), they are the least "
                                             "deadly by a wide margin."
                                             "</ br></ br>From the chart, it "
                                             "appears that the most deadly "
                                             "disasters are natural "
                                             "disasters, with tsunamis, and "
                                             "earthquakes causing the most "
                                             "deaths on average. The health "
                                             "disaster, epidemic, also "
                                             "causes a significant number "
                                             "of deaths.</ br></ br>With "
                                             "this, one can suggest that "
                                             "although the deaths caused by "
                                             "manmade disasters are not "
                                             "insignificant, natural "
                                             "disasters are a much larger "
                                             "problem in Australia.")},
         "bar-type-insured_cost": {"title": ("Most costly disasters "
                                             "in terms of insurance "
                                             "(average)"),
                                   "description": ("The key problem with "
                                                   "this chart, is that the "
                                                   "dataset it is derived "
                                                   "from does not have the "
                                                   "cost data for every "
                                                   "single event. This is "
                                                   "why such disasters like "
                                                   "tsunamis that have "
                                                   "enormous costs indicate "
                                                   "nothing.<br/ ><br />From "
                                                   "the categories that do "
                                                   "have data, hail was "
                                                   "surprisingly the most "
                                                   "expensive disaster in "
                                                   "terms of insurance, with "
                                                   "higher costs than severe "
                                                   "storms and earthquakes."
                                                   "<br /><br />However, "
                                                   "note that the data for "
                                                   "'hail' consititutes hail "
                                                   "disasters, and not "
                                                   "ordinary, harmless "
                                                   "incidents.<br /><br />"
                                                   "Unsurprisingly, urban "
                                                   "fire has had a limited "
                                                   "impact in terms of "
                                                   "insurance cost as they "
                                                   "are typically small in "
                                                   "scale. What is "
                                                   "surprising though, is "
                                                   "the lower than expected "
                                                   "impact of bushfires and "
                                                   "floods.")},
         "line-year": {"title": "Frequency of each disaster type per year",
                       "description": ("From the chart, the first "
                                       "notable suggestion is that "
                                       "the frequency of disasters "
                                       "in Australia is increasing. "
                                       "However, this may not be "
                                       "strictly true. With "
                                       "Australia's growing "
                                       "population, disasters are "
                                       "far more likely to impact "
                                       "more and more Australians "
                                       "with every year, and as "
                                       "this dataset only covers "
                                       "disasters that have an "
                                       "impact on Australia, "
                                       "and/or Australians, the "
                                       "frequency will naturally "
                                       "grow as the population grows "
                                       "and expands across the "
                                       "country.<br /><br />Another "
                                       "point of interest is that "
                                       "the overall decline in "
                                       "shipwreck disasters "
                                       "corresponds with an increase "
                                       "in transport disasters. This "
                                       "is not particularly "
                                       "surprising as many transport "
                                       "disasters are airplane "
                                       "crashes, and with Australia's "
                                       "increasing technological "
                                       "capabilities, it is likely "
                                       "that many journeys previously "
                                       "taken by ship have moved to "
                                       "airplanes.<br /><br />The "
                                       "huge number of shipwrecks in "
                                       "the mid-19th century "
                                       "correspond with both lower "
                                       "technology levels (hence "
                                       "requiring ships), and vast "
                                       "increase in immigration due "
                                       "to the gold rush (thus "
                                       "resulting in more trips by "
                                       "sea).<br /><br />Finally, note "
                                       "that a possible reason for "
                                       "figures being absent before "
                                       "the late 19th century is due "
                                       "to a lack of coverage of such "
                                       "details by the government "
                                       "(from who the dataset is "
                                       "drawed), and is not "
                                       "necessarily a sign of fewer "
                                       "disasters.")}
         }

#Data display options for generic chart generation
#Contains a y-value identifier, with a title
#The description field exists for compatibility purposes, and should be left
#blank
y_cat = {"bar-type-deaths": {"title": "Deaths (average)", "description": ""},
         "bar-type-injuries": {"title": "Injuries (average)",
                               "description": ""},
         "bar-type-evacuated": {"title": "Evacuated (average)",
                                "description": ""},
         "bar-type-insured_cost": {"title": "Insured Cost (average)",
                                   "description": ""},
         "bar-type-homes_destroyed": {"title": "Home(s) Destroyed (average)",
                                      "description": ""},
         "bar-type-buildings_destroyed": {"title": ("Building(s) Destroyed "
                                                    "(average)"),
                                          "description": ""},
         "bar-type-homes_damaged": {"title": "Home(s) Damaged (average)",
                                    "description": ""},
         "bar-type-buildings_destroyed": {"title": ("Building(s) "
                                                    "Destroyed (average)"),
                                          "description": ""},
         "bar-type-aircraft_destroyed": {"title": ("Aircraft Destroyed "
                                                   "(average)"),
                                         "description": ""},
         "bar-type-motor_vechicles_destroyed": {"title": ("Motor Vechicle(s) "
                                                          "Destroyed "
                                                          "(average)"),
                                                "description": ""},
         "bar-type-water_vessels_destroyed": {"title": ("Water Vessel(s) "
                                                        "Destroyed "
                                                        "(average)"),
                                              "description": ""},
         "bar-type-motor_vechicles_damaged": {"title": ("Motor Vechicle(s) "
                                                        "Damaged"),
                                              "description": ""},
         "bar-type-water_vessels_damaged": {"title": ("Water Vessel(s) "
                                                      "Damaged"),
                                            "description": ""},
         "bar-type-livestock_destroyed": {"title": ("Livestock Destroyed "
                                                    "(average)"),
                                          "description": ""},
         "bar-type-government_assistance": {"title": ("Government Assistance "
                                                      "(average)"),
                                            "description": ""}
         }


#Default category for select
default_chart = "bar-type"
default_gen_y = "bar-type-deaths"

#Prefix for generic chart generation
xval_gen_y = "type"
prefix_gen_y = "bar-" + xval_gen_y + '-'

#Source URL for disaster data
source_url = ("http://www.data.gov.au/dataset/"
              "disaster-events-with-category-impact-and-location/"
              "resource/ad5c6594-571e-4874-994c-a9f964d789df")

#Types of data calculation methods for bar graphs
bar_average, bar_frequency = range(2)

#y-values that should have a logarithmic scale when plotted (bar graphs)
bar_log = ("deaths")


def xhtmlHead(title, css):
    """
    Print a xhtml header.
    """
    print ('Content-Type: text/html\n\n'
           '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" '
           '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n'
           '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" '
           'lang="en">\n'
           '<head>\n<meta http-equiv="content-type" content="text/html; '
           'charset=UTF-8" />\n'
           '<link rel="stylesheet" type="text/css" href="{}" '.format(css) +
           '/>\n'
           '<title>{}</title>\n'.format(title) +
           '</head>')


def xhtmlFoot():
    """
    Print a xhtml footer.
    """
    print '</html>'
