#!/usr/bin/env python

"""

An interface for the pivot table, where the user can select the row, column,
display value and filter.

"""

import utils


def displaySelect(name, sel_fields, selected=""):
    """
    Generates an xhtml select/dropdown box with the name, 'name'. Also
    outputs a description of the box, given by title.

    Selected indicates what the default value should be.

    Filter indicates whether the select is a filter for the pivot table.
    """
    print '<select name="{}">'.format(name)

    #Output xhtml
    for k, v in sorted(sel_fields.items(), key=lambda x: x[1]["title"]):
        if k != selected:
            print '<option value="{}">{}</option>'.format(k, v["title"])
        else:
            print ('<option value="{}" '.format(k) +
                   'selected="selected">{}</option>'.format(v["title"]))

    print '</select>'


def displayForms():
    """
    Generate the xhtml forms.
    """
    print ('<p>Please select the desired data comparison:</p>')
    
    #Using get, as post does not function on IVLE
    print '<form method="get" action="{}">'.format(utils.graph_file)

    displaySelect(utils.name_chart, utils.chart, utils.default_chart)
    print '<br />'
    print '<p><input type="submit"/></p>\n</form>'
    
    print '<br />'
    print '<br />'

    print ('<p>Or alternatively, pick a value to compare with all disaster '
           'types (average):</p>')

    print '<form method="get" action="{}">'.format(utils.graph_file)
    displaySelect(utils.name_gen_y, utils.y_cat, utils.default_gen_y)
    print '<br />'
    print '<p><input type="submit"/></p>\n</form>'


def displayDataSource():
    """
    Display the source URL for the data.
    """
    print ('<p class="source"><em>Data used for the pivot table can be found '
           '<a href="{}">here</a></em></p>'.format(utils.source_url))


def displayBody():
    """
    Display the body of an xhtml page.
    """
    print '<body>'
    print '<h1>Charts for Disaster Events</h1>'
    displayForms()
    displayDataSource()
    print '</body>'


def displayPage():
    """
    Display the xhtml page.
    """
    utils.xhtmlHead(utils.inter_title, "../style/interface.css")
    displayBody()
    utils.xhtmlFoot()


def main():
    displayPage()

if __name__ == "__main__":
    main()
