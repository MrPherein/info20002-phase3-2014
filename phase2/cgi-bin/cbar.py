#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

import cStringIO
import utils
import csv
import math
import cgi

from collections import defaultdict

# Enable pretty exceptions! Remove in final version.
#import sys
#import cgitb
#cgitb.enable()
#sys.stderr = sys.stdout

header_norm = {
    "id"                            :   "id",
    "resourceType"                  :   "resource_type",
    "title"                         :   "type",
    "description"                   :   "description",
    "startDate"                     :   "start_date",
    "endDate"                       :   "end_date",
    "lat"                           :   "lat",
    "lon"                           :   "lon",
    "documentType"                  :   "document_type",
    "author"                        :   "author",
    "Evacuated"                     :   "evacuated",
    "Homeless"                      :   "homeless",
    "Injuries"                      :   "injuries",
    "Deaths"                        :   "deaths",
    "Insured Cost"                  :   "insured_cost",
    "Train(s) damaged"              :   "trains_damaged",
    "Train(s) destroyed"            :   "trains_destroyed",
    "Home(s) damaged"               :   "homes_damaged",
    "Home(s) destroyed"             :   "homes_destroyed",
    "Building(s) damaged"           :   "buildings_damaged",
    "Building(s) destroyed"         :   "buildings_destroyed",
    "Ind Premises destroyed"        :   "ind_premises_destroyed",
    "Com Premises damaged"          :   "com_premises_damaged",
    "Com Premises destroyed"        :   "com_premises_destroyed",
    "Bridge(s) damaged"             :   "bridges_damaged",
    "Bridge(s) destroyed"           :   "bridges_destroyed",
    "Aircraft damaged"              :   "aircraft_damaged",
    "Aircraft destroyed"            :   "aircraft_destroyed",
    "Motor Vehicle(s) damaged"      :   "motor_vechicles_damaged",
    "Motor Vehicle(s) destroyed"    :   "motor_vechicles_destroyed",
    "Water vessel(s) damaged"       :   "water_vessels_damaged",
    "Water vessel(s) destroyed"     :   "water_vessels_destroyed",
    "Business(es) damaged"          :   "businesses_damaged",
    "Business(es) destroyed"        :   "businesses_destroyed",
    "Farm(s) damaged"               :   "farms_destroyed",
    "Farm(s) destroyed"             :   "farms_destroyed",
    "Crop(s) destroyed"             :   "crops_destroyed",
    "Livestock destroyed"           :   "livestock_destroyed",
    "Government assistance"         :   "government_assistance",
    "regions"                       :   "region",
    "subjects"                      :   "subjects",
    "url"                           :   "url",
    "resourceUrl"                   :   "resource_url",
}

state_trans = {
    "Australian Capital Territory"  :   "ACT",
    "New South Wales"               :   "NSW",
    "Northern Territory"            :   "NT",
    "Outside Australia"             :   "OA",
    "Queensland"                    :   "QLD",
    "South Australia"               :   "SA",
    "Tasmania"                      :   "TAS",
    "Victoria"                      :   "VIC",
    "Western Australia"             :   "WA",
}

def getData(csvPath):
    col = 'type'
    row = 'region'

    row_data = defaultdict(lambda: defaultdict(int))
    with open(csvPath,"rb") as csvfile:
        reader = csv.DictReader(csvfile)

        # Get data
        for line in reader:
            for citem in line[col].split(";"):
                for ritem in line[row].split(";"):
                    row_data[ritem][citem] += int(line['deaths'])

    return row_data

def generateGraph(csvPath,sel_dis=["Cyclone","Environmental","Industrial"]):
    data = getData(csvPath)
    states = sorted(data.keys())

    # Rawr.
    states.remove("Outside Australia")
    vals = []

    for key in sel_dis:
        vals.append([])

    for state in states:
        ind = 0
        for diaster in sel_dis:
            vals[ind].append(data[state][diaster])
            ind += 1

    #print states
    #print sel_dis
    #print vals

    for i in range(0,len(states)):
        states[i] = state_trans[states[i]]

    gMax = 0
    for vlist in vals:
        for val in vlist:
            gMax = max(gMax,val)
    gMax = math.ceil(gMax / 500.0) * 500.0

    fig = plt.figure(figsize=(16,7), dpi=120)
    ax = fig.add_subplot(111)

    ## necessary variables
    ind = np.arange(len(states))                # the x locations for the groups
    width = 1.0 / (len(sel_dis) + 1)                     # the width of the bars

    colors = ['black','red','green','purple','orange','yellow','blue','brown','gray']
    # The bars
    rect_list = []
    for i in range(0,len(sel_dis)):
        rect_list.append(ax.bar(ind+width*i, vals[i], width,color=colors[i]))

    # axes and labels
    ax.set_xlim(-width,width+len(states))
    ax.set_ylim(0,gMax)
    ax.set_ylabel('Deaths')
    ax.set_xlabel('States')
    #ax.set_title('Deaths by state and disaster')
    xTickMarks = states
    ax.set_xticks(ind+width*(len(sel_dis)/2.0))
    xtickNames = ax.set_xticklabels(xTickMarks)
    plt.setp(xtickNames, rotation=0, fontsize=12)

    ## add a legend
    ax.legend(rect_list, sel_dis)

    # Save figure
    plt.savefig(utils.img_dir + "cbar-state-death.png",
                bbox_inches='tight')

def main():
    form = cgi.FieldStorage()

    #utils.xhtmlHead(utils.inter_title,"../style/chart.css")
    dis_type = form.getlist("type")
    if(len(dis_type) > 0):
        generateGraph(utils.csv_file,dis_type)
    else:       
        generateGraph(utils.csv_file)
    #utils.xhtmlFoot()
    
if __name__ == "__main__":
    main()
