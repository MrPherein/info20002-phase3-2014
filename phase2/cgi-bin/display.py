#!/usr/bin/env python

"""

Generates an xhtml page that generates and displays a chart. The chart
generated and displayed is based upon the cgi element received by
the program.

"""

import cgi
import utils

#matplotlib imported and used here insted to prevent conflicts with the
#following imports
import matplotlib
matplotlib.use('Agg')

import bar
import cbar
import line


def get_input():
    """
    Read the cgi, and returns the value of the chart determining
    input.
    """
    form = cgi.FieldStorage()
    hypothesis_chart = form.getfirst(utils.name_chart, "")
    gen_chart = form.getfirst(utils.name_gen_y, "")

    #Determine what kind of data to use
    if hypothesis_chart:
        plot_type = hypothesis_chart
        metadata = utils.chart
    elif gen_chart:
        plot_type = gen_chart
        metadata = utils.y_cat
    else:
        plot_type = ""
        metadata = {}
    return metadata, plot_type


def display_error():
    """
    Displays an error page.
    """
    print '<h1>Error. Did you press the submit button?</h1>'


def display_image(metadata, plot_type):
    """
    Display a chart image along with a title based on the
    input.
    """
    print '<h1>{}</h1>'.format(metadata[plot_type]["title"])
    print '<img src="{}" alt="chart" />'.format(utils.img_dir + plot_type +
                                                utils.img_extension)


def display_conclusions(metadata, plot_type):
    """
    Display conclusions for a given chart.
    """
    if metadata[plot_type]["description"]:
        print '<p>{}</p>'.format(utils.chart[plot_type]["description"])


def display_chart(metadata, plot_type):
    """
    Display a a chart and conclusions regarding it.
    """
    display_image(metadata, plot_type)
    display_conclusions(metadata, plot_type)


def display_plot():
    """
    Plots a chart and displays it with xhtml, based on
    the cgi provided.
    """
    metadata, plot_type = get_input()

    utils.xhtmlHead("Disaster Chart", "../style/chart.css")

    print '<body>'
    print '<div class="plot">'

    segments = plot_type.split('-')
    chart_type = segments[0]

    #Frequency bar chart - no y segment given
    if chart_type == "bar" and len(segments) == 2:
        bar.generateGraph(utils.csv_file, utils.bar_frequency, segments[1])
        display_chart(metadata, plot_type)

    #Average bar chart
    elif chart_type == "bar" and len(segments) == 3:
        bar.generateGraph(utils.csv_file, utils.bar_average, segments[1],
                          segments[2])
        display_chart(metadata, plot_type)

    #Clustered bar chart
    elif chart_type == "cbar":
        cbar.generateGraph(utils.csv_file)
        display_chart(metadata, plot_type)

    #Line chart
    elif chart_type == "line":
        line.generateGraph(utils.csv_file)
        display_chart(metadata, plot_type)

    #Invalid input
    else:
        display_error()

    print '</div>'
    print '</body>'
    utils.xhtmlFoot()


def main():
    display_plot()


if __name__ == "__main__":
    main()
