"""
generateGraph()
Takes as input disaster_processed.csv, plots frequency of disaster by decade
for  each type of disaster found in file then outputs figure to file
"line_year.png"
"""

#From the Introduction to Informatics LMS
# --- fix to IVLE's problem in using matplotlib
import os, sys
os.dup2(2, 3)
stderr = os.fdopen(2, 'a')
stderr.close()
# ---
import matplotlib

#Commented out, as it's run by the calling program but would be required to
#run by itself
#matplotlib.use('Agg')

from pylab import *
# --- fix to IVLE's problem in using matplotlib
os.dup2(3, 2)
sys.__stderr__ = sys.stderr = os.fdopen(2, 'a')
# ---

import csv
import utils

def decade(year):
    return (year/10)*10

def read_data(filename):
    index_type = 0
    index_year = -1
    data = []
    with open(filename, "rb") as csvfile:
        reader = csv.reader(csvfile)
        for i, line in enumerate(reader):
            if(i != 0):
                data.append([line[index_type], int(line[index_year])])
    return data

def generateGraph(csvfile):
    data = read_data(csvfile)
    unique_years = []
    unique_types = []
    clf()
    markers = ['^', 's', 'D']
    for line in data:
        if not (line[1] in unique_years):
            unique_years.append(line[1])
        if not (line[0] in unique_types):
            unique_types.append(line[0])
    unique_years.sort()
    axis_x = range(decade(unique_years[0]),decade(unique_years[-1])+1,10)
    for i in range(0, len(unique_types)):
        freqs = {key: 0 for key in axis_x}
        for j in data:
            if j[0] == unique_types[i]:
                freqs[decade(j[1])] += 1
        lines = plot(axis_x, [freqs[a] for a in sorted(freqs.keys())])
        setp(lines, 'marker', markers[i/7])
    
    legend(unique_types, loc='lower right', numpoints=1,
           prop=matplotlib.font_manager.FontProperties(size='smaller'))
    
    xlabel("Year by decade")
    ylabel("Frequency")
    xticks(axis_x, [str(a) for a in axis_x], rotation='90')
    xlim( decade(unique_years[0]), decade(unique_years[-1])+60 )
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(18,10)
    axes().xaxis.grid(True, color=(0.8,0.8,0.8), linestyle="-", which='major')
    plt.savefig(utils.img_dir + "line-year.png", dpi = 100, bbox_inches='tight')
    
generateGraph(utils.csv_file)

