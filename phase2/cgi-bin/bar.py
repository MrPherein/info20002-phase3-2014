#!/usr/bin/env python

"""

Generates bar graphs from an input csv file, that displays either the amount
of disasters per state, the average amount of deaths per disaster type, or
the most costly disaster type.

"""

#From the Introduction to Informatics LMS
# --- fix to IVLE's problem in using matplotlib
import os, sys
os.dup2(2, 3)
stderr = os.fdopen(2, 'a')
stderr.close()
# ---
import matplotlib

#Commented out, as it's run by the calling program but would be required to
#run by itself
#matplotlib.use('Agg') 

from pylab import *
# --- fix to IVLE's problem in using matplotlib
os.dup2(3, 2)
sys.__stderr__ = sys.stderr = os.fdopen(2, 'a')
# ---

import utils
import csv

from collections import defaultdict


def parseDataFrequency(loc_csv, xtype):
    """
    Parses data for frequency of a column in a csv file.
    """
    with open(loc_csv) as csvfile:
        csvdict = csv.DictReader(csvfile)
        freq = defaultdict(int)

        #Parse the data
        for line in csvdict:

            #Multiple values for a column in the csv fle are ';' deliminated
            for x in line[xtype].split(';'):
                freq[x] += 1

        return freq


def parseDataAverage(loc_csv, xtype, ytype):
    """
    Parses data for when an average over all the values are desired. The
    ytype must correspond to a numeral column in the csv.
    """
    with open(loc_csv) as csvfile:
        csvdict = csv.DictReader(csvfile)
        data = defaultdict(lambda: defaultdict(int))

        #Parse the data
        for line in csvdict:

            #Multiple values for a column in the csv fle are ';' deliminated
            for x in line[xtype].split(';'):
                data[x]["total"] += int(line[ytype])
                data[x]["freq"] += 1

        #Find the average
        average = {}
        for k in data:
            average[k] = float(data[k]["total"]) / data[k]["freq"]

        return average


def parseData(loc_csv, gtype, xtype, ytype):
    """
    Parses a csv for plot data.
    """
    if gtype == utils.bar_average:
        return parseDataAverage(loc_csv, xtype, ytype)
    elif gtype == utils.bar_frequency:
        return parseDataFrequency(loc_csv, xtype)


def generateGraph(loc_csv, gtype, xtype, ytype=""):
    """
    Generates a bar graph from input csv file.
    """
    #Read in data
    data = parseData(loc_csv, gtype, xtype, ytype)
    xvalues = []
    yvalues = []
    for k, v in sorted(data.items()):  # TODO - make neater - adjust earlier
        if k == "Complex Emergencies":
            xvalues.append("Cplx Emerge.")
        else:
            xvalues.append(k)
        yvalues.append(v)

    clf()

    #Label axes
    fig = plt.figure()
    
    plt.xlabel(" ".join(xtype.split("_")).capitalize())

    if gtype == utils.bar_frequency:
        plt.ylabel("Frequency")
    else:
        plt.ylabel(" ".join(ytype.split("_")).capitalize())

    #Plot the graph
    bar(arange(len(yvalues)), yvalues, align="center",
        log=(ytype in utils.bar_log))
    xticks(arange(len(xvalues)), xvalues, rotation=65)
    gcf().subplots_adjust(bottom=0.30)
    plt.autoscale()

    #Output graph
    if gtype == utils.bar_average:
        savefig(utils.img_dir + "bar-{}-{}.png".format(xtype, ytype), dpi=120)
    else:
        savefig(utils.img_dir + "bar-{}.png".format(xtype), dpi=120)
