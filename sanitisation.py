import csv

"""

Takes disaster_original.csv as input, writes a file disaster_processed.csv.
Output file contains columns headed by those specified in that other document.
An attempt has been made at standardising dates provided to a DD/MM/YYYY format
but there will be errors for reasons below.
Cyclones retain their names for now but should not be too difficult to remove
if necessary.
Also supposing that IVLE will have the csv library.

The given data set is difficult to standardise as data entry conventions
are quite varied across listed incidents.

For example, the date is given in both MM/DD/YYYY and DD/MM/YYYY formats
throughout the data. Some of the resulting ambiguity can be resolved by
inspection of the numerical values (by property of the Gregorian Calendar,
there are twelve months in a year, for example, and hence any value that
exceeds that amount cannot qualify as a month) or by inspection of the string
stored in the column headed by "description" as it often is the case that
the description clarifies by naming a month.

However, there are entries that betray even this standard. Entry id 60, for
example, lists its date as "12/09/1909", makes no mention of any month in its
description - though it does mention that it was Summer at the time - and
instead opts to include the detail in its title "Environmental - Heatwave Dec
1909" instead. To account for every temporal cue such as seasons, or every
symbol to represent a month or other helpful detail would be ungainly, and hence
this program will make no further pursuit of robustness.

"""

months = {
    "January"   :   "01",
    "February"  :   "02",
    "March"     :   "03",
    "April"     :   "04",
    "May"       :   "05",
    "June"      :   "06",
    "July"      :   "07",
    "August"    :   "08",
    "September" :   "09",
    "October"   :   "10",
    "November"  :   "11",
    "December"  :   "12",
    }

header_norm = {
    "id"                            :   "id",
    "resourceType"                  :   "resource_type",
    "title"                         :   "type",
    "description"                   :   "description",
    "startDate"                     :   "start_date",
    "endDate"                       :   "end_date",
    "lat"                           :   "lat",
    "lon"                           :   "lon",
    "documentType"                  :   "document_type",
    "author"                        :   "author",
    "Evacuated"                     :   "evacuated",
    "Homeless"                      :   "homeless",
    "Injuries"                      :   "injuries",
    "Deaths"                        :   "deaths",
    "Insured Cost"                  :   "insured_cost",
    "Train(s) damaged"              :   "trains_damaged",
    "Train(s) destroyed"            :   "trains_destroyed",
    "Home(s) damaged"               :   "homes_damaged",
    "Home(s) destroyed"             :   "homes_destroyed",
    "Building(s) damaged"           :   "buildings_damaged",
    "Building(s) destroyed"         :   "buildings_destroyed",
    "Ind Premises destroyed"        :   "ind_premises_destroyed",
    "Com Premises damaged"          :   "com_premises_damaged",
    "Com Premises destroyed"        :   "com_premises_destroyed",
    "Bridge(s) damaged"             :   "bridges_damaged",
    "Bridge(s) destroyed"           :   "bridges_destroyed",
    "Aircraft damaged"              :   "aircraft_damaged",
    "Aircraft destroyed"            :   "aircraft_destroyed",
    "Motor Vehicle(s) damaged"      :   "motor_vechicles_damaged",
    "Motor Vehicle(s) destroyed"    :   "motor_vechicles_destroyed",
    "Water vessel(s) damaged"       :   "water_vessels_damaged",
    "Water vessel(s) destroyed"     :   "water_vessels_destroyed",
    "Business(es) damaged"          :   "businesses_damaged",
    "Business(es) destroyed"        :   "businesses_destroyed",
    "Farm(s) damaged"               :   "farms_destroyed",
    "Farm(s) destroyed"             :   "farms_destroyed",
    "Crop(s) destroyed"             :   "crops_destroyed",
    "Livestock destroyed"           :   "livestock_destroyed",
    "Government assistance"         :   "government_assistance",
    "regions"                       :   "region",
    "subjects"                      :   "subjects",
    "url"                           :   "url",
    "resourceUrl"                   :   "resource_url",
}

def std_date(date, description):
    puncts = '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~'
    output= date.split(" ")[0].split("/")
    MONTH = ''
    for i in range(0, 2):
        if len(output[i]) < 2:
            output[i] = "0" + output[i]
    if int(output[1]) > 12:
        temp = output[0]
        output[0] = output[1]
        output[1] = temp
    for item in description.strip().split():
        if item.strip(puncts) in months.keys():
            MONTH = item.strip(puncts)
            break
    if MONTH != '':
        if output[0] == months[MONTH]:
            temp = output[0]
            output[0] = output[1]
            output[1] = temp
    return str(output[0]) + "/" + str(output[1]) + "/" + str(output[2])

def std_title(title):
    if "-" in title:    # eg Hail - Orange
        return title.split("-")[0].strip().split(",")[0].strip()
    else:               # eg Cyclone Justin
        return title.split(" ")[0].strip()

def get_year(date):
    # Input in dd/mm/yyyy
    return date.split('/')[2].strip()

def main():
    data = []
    with open("disaster_original.csv", "rb") as csv_f:
        read = csv.reader(csv_f)
        for item in read:
            data.append(item)

    for i in range(1, len(data)):
        data[i][4] = std_date(data[i][4], data[i][3])
        data[i][5] = std_date(data[i][5], data[i][3])
        data[i][2] = std_title(data[i][2])
        data[i].append(get_year(data[i][4]))    # Add new year column

    # Update column header names
    for i in range(0, len(data[0])):
        if data[0][i] in header_norm.keys():
            data[0][i] = header_norm[data[0][i]]
        else:
            print "WARNING: Column '{}' has no normalization data.".format(data[0][i])

    # Add new column headers
    data[0].append('year')

    # HELPER: Print out data elements with their indexes
    #ind = 0
    #for item in data[0]:
    #    print (item,ind)
    #    ind += 1

    with open("disaster_processed.csv", "wb") as csv_f:
        write = csv.writer(csv_f)
        for row in data:
            output = []
            for i in range(1, 44):
                if (i == 2) or (i == 4) or (i == 5) or (i >= 10 and i < 40) or (i == 43):
                    # If data is blank then print a 0 instead of a blank space
                    if len(row[i]) == 0:
                        row[i] = 0
                    output.append(row[i])
            write.writerow(output)

if __name__ == "__main__":
    main()

